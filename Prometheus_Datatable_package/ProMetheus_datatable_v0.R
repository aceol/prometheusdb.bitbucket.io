##############
#Formatting function for row details - modify as you need 
format <-  function(d) { # `d` is the original data object for the row
  tmp <- strsplit(d['Peptides'],';')[[1]]
  tmpdf <- data.frame()
  for (p in tmp) {
    tmpdf <- rbind(tmpdf, RK.methylpeps[RK.methylpeps$PEPTIDE==p,])
  }
  tmpdf <- tmpdf[,!apply(tmpdf, 2 , all.nas)]
  
  TAB <-'<table cellpadding=\"5\" cellspacing=\"0\" border=\"0\">'
  TAB <- paste(TAB,'<trstyle="text-align:center">','<th>#</th>')
  for (cname in colnames(tmpdf)) {
    TAB <- paste(TAB,'<th>',cname,'</th>')
  }
  TAB <- paste(TAB,'</tr>')
  n = 0
  for (i in 1:dim(tmpdf)[1]) {
    n <- n+1
    TAB <- paste(TAB,'<tr style="text-align:center"><td>',n,'</td>')
    for (j in tmpdf[i,]) {
      if (is.na(j)) {
        k <- '<td></td>'
      } else if (j==-1) {
        k <- '<td style="background-color:#FF5C4D;">&darr;</td>'
      } else if (j==0) {
        k <- '<td style="background-color:#FFCD58;">&harr;</td>'
      } else if (j==1) {
        k <- '<td style="background-color:#DAD870;">&uarr;</td>'
      } else {
        k <- paste('<td>',j,'</td>')
      }
      TAB <- paste(TAB,k)
    }
    TAB <- paste(TAB,'</tr>')
  }
  TAB <- paste(TAB,'</table>')
  return(TAB)
}

all.nas <- function(v) {
  return(sum(is.na(v))==length(v))
}

#################
# setwd to source file location
# remotes::install_github('rstudio/DT')
library(DT)
library(readr)

RK.methylsites <- data.frame(read_delim("20211026_ProMetheus_methylsites.txt",
                                        "\t", escape_double = FALSE, trim_ws = TRUE))
RK.methylsites$Gene <- as.factor(RK.methylsites$Gene)
RK.methylsites$LeadProt <- as.factor(RK.methylsites$LeadProt)
RK.methylsites$Residue <- as.factor(RK.methylsites$RES)
RK.methylsites$Modification <- as.factor(RK.methylsites$MOD)
RK.methylsites$Position <- as.integer(RK.methylsites$POS)
for (i in colnames(RK.methylsites)[9:13]) {
  RK.methylsites[[i]] <- as.factor(RK.methylsites[[i]])
}
#str(RK.methylsites)

####################
RK.methylpeps <- data.frame(read_delim("20211026_ProMetheus_methylpeps.txt", 
                                       "\t", escape_double = FALSE, trim_ws = TRUE))
RK.methylpeps <- RK.methylpeps[,c(1,10:11,16:38)]
RK.methylpeps <- RK.methylpeps[!duplicated(RK.methylpeps$PEPTIDE), ]

#############
RK.methylsites2 <- cbind(' ' = '&oplus;', RK.methylsites[,c(2,1,14:16,6:13)])
RK.methylsites2$TAB <- apply(RK.methylsites2, 1, format)
datatable(RK.methylsites2, escape=F, rownames = FALSE,
          class = 'cell-border stripe',
          filter = 'top',
          caption = 'R/K-methyl-sites. Filters are case-sensitive and support Regular Expressions.',
          options = list(initComplete = JS("function(settings, json) {",
                                           "$('body').css({'font-family': 'Calibri'});",
                                           "}"), 
                         search = list(regex = TRUE,caseInsensitive = FALSE),
                         columnDefs = list(list(visible = FALSE, 
                                                targets = c(8,14)),
                                           list(orderable = FALSE,
                                                className = 'details-control', 
                                                targets = 0))),
          callback = JS("table.column(0).nodes().to$().css({cursor: 'pointer'});
                        var format = function(d) {
                        return d[14];
                        };
                        table.on('click', 'td.details-control', function() {
                        var td = $(this), row = table.row(td.closest('tr'));
                        if (row.child.isShown()) {
                        row.child.hide();
                        td.html('&oplus;');
                        } else {
                        row.child(format(row.data())).show();
                        td.html('&CircleMinus;');
                        }
                        });"))
